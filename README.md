# v2-sitemap plugin for Kirby 3

> Just an easy sitemap plugin for Kirby 3

****

## Installation

### Download

Download and copy this repository to `/site/plugins/v2-sitemap`.

### Composer

```
composer require vitamin2/v2-sitemap
```

## Options
templates that should be ignored at to your config.php

```php
// ignored templates in sitemap
'sitemap.ignore' => ['error'],
```

## License

MIT
